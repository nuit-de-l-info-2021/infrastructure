provider "scaleway" {
  zone            = "fr-par-1"
  region          = "fr-par"
  # access_key = $SCW_ACCESS_KEY
  # secret_key = $SCW_SECRET_KEY
  # project_id = $SCW_SECRET_KEY
}

provider "kubernetes" {
  host  = null_resource.kubeconfig.triggers.host
  token = null_resource.kubeconfig.triggers.token
  cluster_ca_certificate = base64decode(
    null_resource.kubeconfig.triggers.cluster_ca_certificate
  )
}

provider "gitlab" {
  # token = $GITLAB_TOKEN
}

provider "random" {
  # Configuration options
}

provider "null" {
  # Configuration options
}
