resource "scaleway_k8s_cluster" "ndl-cluster" {
  name    = "ndl2021"
  version = "1.22.3"
  cni     = "cilium"
}

resource "scaleway_k8s_pool" "ndl-pool" {
  cluster_id          = scaleway_k8s_cluster.ndl-cluster.id
  name                = "pool"
  node_type           = "dev1-m"
  size                = 3
  autohealing         = true
}

resource "null_resource" "kubeconfig" {
  depends_on = [scaleway_k8s_pool.ndl-pool] # at least one pool here
  triggers = {
    host                   = scaleway_k8s_cluster.ndl-cluster.kubeconfig[0].host
    token                  = sensitive(scaleway_k8s_cluster.ndl-cluster.kubeconfig[0].token)
    cluster_ca_certificate = scaleway_k8s_cluster.ndl-cluster.kubeconfig[0].cluster_ca_certificate
  }
}