resource "gitlab_group_variable" "kube-url" {
   group   = "14345082"
   key       = "KUBE_URL"
   value     = null_resource.kubeconfig.triggers.host
   protected = false
   masked    = false
}

resource "gitlab_group_variable" "kube-ca-pem-file" {
   group   = "14345082"
   key       = "KUBE_CA_PEM_FILE"
   value     = base64decode(null_resource.kubeconfig.triggers.cluster_ca_certificate)
   protected = false
   masked    = false
   variable_type = "file"
}

resource "gitlab_group_variable" "kube-token" {
   group   = "14345082"
   key       = "KUBE_TOKEN"
   value     = null_resource.kubeconfig.triggers.token
   protected = true
   masked    = true
}
