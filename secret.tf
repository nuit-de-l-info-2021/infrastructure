resource "kubernetes_secret" "example" {
  metadata {
    name = "api-nestjs"
  }

  data = {
    JWT_SECRET         = random_password.jwt-secret.result
    JWT_REFRESH_SECRET = random_password.jwt-refresh-secret.result
    DB_PASSWORD        = random_password.db_app_password.result
  }
}

resource "random_password" "jwt-secret" {
  length  = 16
  special = true
}

resource "random_password" "jwt-refresh-secret" {
  length  = 16
  special = true
}
