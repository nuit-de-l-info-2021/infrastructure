resource "scaleway_rdb_instance" "database" {
  name              = "database"
  node_type         = "db-dev-s"
  engine            = "PostgreSQL-13"
  is_ha_cluster     = false
  disable_backup    = true
  volume_type       = "bssd"
  volume_size_in_gb = 5
}

resource "scaleway_rdb_database" "production" {
  instance_id    = scaleway_rdb_instance.database.id
  name           = "production"
}

resource "random_password" "db_admin_password" {
  length  = 16
  special = true
}

resource "scaleway_rdb_user" "db_admin" {
  instance_id = scaleway_rdb_instance.database.id
  name        = "admin"
  password    = random_password.db_admin_password.result
  is_admin    = true
}

resource "random_password" "db_app_password" {
  length  = 16
  special = true
}

resource "scaleway_rdb_user" "db_app" {
  instance_id = scaleway_rdb_instance.database.id
  name        = "app"
  password    = random_password.db_app_password.result
}
